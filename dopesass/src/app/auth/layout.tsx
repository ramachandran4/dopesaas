import Image from 'next/image'

const layout = ({ children }: any) => {
    return (
        <div className='w-full fixed'>
            <div className='flex'>
                <div className='w-1/2 bg-gray-300 image py-16 px-10'>
                    <h1 className='w-[30%]'>
                        <a href="#"><Image src={require("../../../public/assets/images/logo.svg").default} width={100} height={100} alt='logo' /></a>
                    </h1>
                    <div className='mt-[32rem]'>
                        <h1 className='text-white text-[40px] Inter-SemiBold'>Numquam architecto iure</h1>
                        <p className='text-white Inter-Medium w-[60%]'>Ut corrupti est molestiae occaecati <br /> voluptatem vel harum explicabo numquam.</p>
                    </div>
                </div>
                <div className='w-1/2'>{children}</div>
            </div>
        </div>
    )
}

export default layout
