"use client"

import Button from '@/app/_components/ui/Button'
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'

const page = () => {
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');

	const handleFirstNameChange = (event:any) => {
		const value = event.target.value;
		if (/^[a-zA-Z]*$/.test(value) || value === '') {
			setFirstName(value);
		}
	};

	const handleLastNameChange = (event:any) => {
		const value = event.target.value;
		if (/^[a-zA-Z]*$/.test(value) || value === '') {
			setLastName(value);
		}
	};

	const [password, setPassword] = useState('');
	const [showPassword, setShowPassword] = useState(false);

	const handleTogglePassword = () => {
		setShowPassword((prevShowPassword) => !prevShowPassword);
	};

	return (
		<div className='pt-20 pb-16 px-10 w-full'>
			<div className="wrapper">
				<div className='w-full'>
					<h1 className='text-[32px] Inter-Bold text-[#040308]'>Create account</h1>
					<div className='flex items-center mb-14 gap-1'>
						<h5 className='text-base Inter-Medium'>Already have an acccount?</h5>
						<Link href="/auth/login" className='text-[#312ECB]'>Login</Link>
					</div>
					<form action="" className=''>
						<div className='flex justify-between mb-5'>
							<div className="w-[48%]">
								<input type="text" placeholder='First Name' value={firstName} onChange={handleFirstNameChange} className='border-2 border-solid border-[#808080] w-full py-3 px-3 rounded-lg' />
							</div>
							<div className="w-[48%]">
								<input type="text" placeholder='Last Name' value={lastName} onChange={handleLastNameChange} className='border-2 border-solid border-[#808080] w-full py-3 px-3 rounded-lg' />
							</div>
						</div>
						<ul className='mb-5'>
							<li><input type="email" placeholder='Email' className='border-2 border-solid border-[#808080] w-full py-3 px-3 rounded-lg mb-5' /></li>
							<li><div className='flex items-center'>
								<div className='flex items-center gap-2 border-2 border-solid border-[#808080] py-3 px-3 w-full rounded-lg '>
									<input type={showPassword ? 'text' : 'password'} value={password} onChange={(e) => setPassword(e.target.value)} id='password' placeholder='Password' maxLength={16} className='w-full' />
									<div className='w-5'>
										<Image
											// src={showPassword ? '/hide.svg' : '/eye.svg'}
											alt={showPassword ? 'Hide password' : 'Show password'}
											onClick={handleTogglePassword}
											style={{ cursor: 'pointer' }}
											src={require("../../../../public/assets/images/eye.svg").default} width={100} height={65} />
									</div>
								</div>
							</div></li>
						</ul>
						<div className='flex gap-5 mb-5'>
							<input type="checkbox" className='w-4' />
							<p className='text-base Inter-Medium text-[#040308]'>I agree to DopSass <Link href="#" className='text-[#312ECB]'>Terms of service and </Link>and <Link href="#" className='text-[#312ECB]'>Privacy policy</Link></p>
						</div>
						<Button name="Create Account" />
					</form>
					<div className='flex w-full gap-3 items-center my-4'>
						<hr className='w-[97%] border-solid border border-[#808080]' />
						<h5 className='Inter-Medium text-xl'>or</h5>
						<hr className='w-[97%] border-solid border border-[#808080]' />
					</div>
					<div>
						<div className='flex justify-center gap-2 py-3 border-[1px] border-solid rounded-lg mb-5'>
							<div className='w-6'>
								<Image src={require("../../../../public/assets/images/google.svg").default} width={95} height={55} alt='google' />
							</div>
							<Link href="https://www.google.com/" className='text-xl Inter-SemiBold'>Continue with Google</Link>
						</div>
						<div className='flex justify-center gap-2 py-3 border-[1px] border-solid rounded-lg'>
							<div className='w-6'>
								<Image src={require("../../../../public/assets/images/apple.svg").default} width={95} height={55} alt='google' />
							</div>
							<Link href="https://www.appstore.com" className='text-xl Inter-SemiBold'>Continue with Apple</Link>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default page

