"use client"

import Button from '@/app/_components/ui/Button'
import Image from 'next/image'
import Link from 'next/link'
import React, { useState } from 'react'


const page = () => {

	const [password, setPassword] = useState('');
	const [showPassword, setShowPassword] = useState(false);

	const handleTogglePassword = () => {
		setShowPassword((prevShowPassword) => !prevShowPassword);
	};

	// const togglePasswordVisibility = () => {
	// 	setShowPassword(!showPassword);
	// };

	return (
		<div className='pt-20 pb-16 px-10 w-full'>
			<div className="wrapper">
				<div className='w-full'>
					<h1 className='text-[32px] Inter-Bold text-[#040308] mb-3'>Welcome Back</h1>
					<div className='flex items-center mb-14 gap-1'>
						<h5 className='text-base Inter-Medium'>Don’t have an account?</h5>
						<Link href="/auth/signup" className='text-[#312ECB]'>Sign Up</Link>
					</div>
					<form action="" className='mb-5'>
						<ul>
							<li><input type="email" placeholder='Email' className='border-2 border-solid border-[#808080] w-full py-3 px-3 rounded-lg mb-5' /></li>
							<li><div className='flex items-center'>
								<div className='flex items-center gap-2 border-2 border-solid border-[#808080] py-3 px-3 w-full rounded-lg'>
									<input type={showPassword ? 'text' : 'password'} value={password} onChange={(e) => setPassword(e.target.value)} id='password' placeholder='Password' maxLength={16} className='w-full' />
									<div className='w-5'>
										<Image
											// onClick={togglePasswordVisibility}
											alt={showPassword ? 'Hide password' : 'Show password'}
											onClick={handleTogglePassword}
											style={{ cursor: 'pointer' }} 
											src={require("../../../../public/assets/images/eye.svg").default} width={100} height={65}
										/>
									</div>
								</div>
							</div></li>
							<div className='text-end my-4'>
								<Link href="#" className='text-sm text-[#312ECB] Inter-Medium'>Forgot Password</Link>
							</div>
						</ul>
						<div className='py-3 px-28 bg-[#312ECB] w-full rounded-lg text-center'>
							<Link href="/" className='text-white Inter-SemiBold' >Login</Link>
						</div>
						{/* <Button name="Login" /> */}
					</form>
					<div className='flex w-full gap-3 items-center my-4'>
						<hr className='w-[97%] border-solid border border-gray-300' />
						<h5 className='Inter-Medium text-xl'>or</h5>
						<hr className='w-[97%] border-solid border border-gray-300' />
					</div>
					<div>
						<div className='flex justify-center gap-2 py-3 border-[1px] border-solid rounded-lg mb-5'>
							<div className='w-6'>
								<Image src={require("../../../../public/assets/images/google.svg").default} width={95} height={55} alt='google' />
							</div>
							<Link href="https://www.google.com/" className='text-xl Inter-SemiBold'>Continue with Google</Link>
						</div>
						<div className='flex justify-center gap-2 py-3 border-[1px] border-solid rounded-lg'>
							<div className='w-6'>
								<Image src={require("../../../../public/assets/images/apple.svg").default} width={95} height={55} alt='google' />
							</div>
							<Link href="https://www.appstore.com" className='text-xl Inter-SemiBold'>Continue with Apple</Link>
						</div>
					</div>
				</div>
			</div>
		</div>
	)
}

export default page

