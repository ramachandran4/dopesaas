import React from 'react'


const Home = () => {
    return (
        <div>
            <div className='w-full h-[100vh] fixed flex'>
                <div className='image w-1/2 h-full'></div>
                <div className='bg-white w-1/2 h-full'>
                    <h1 className='text-black text-7xl Inter-Bold py-56 px-16 text-center border-solid'>🙋‍♂️Hello Ashwin Chetta...</h1>
                </div>
            </div>
        </div>
    )
}

export default Home
