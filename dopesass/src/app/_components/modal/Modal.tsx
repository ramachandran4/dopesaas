"use client"

import Link from 'next/link';
import React, { useState } from 'react'

const Modal = () => {
    const [LoginModal, setLoginModal] = useState(true);
    const handleClose = () => {
        setLoginModal(false);
    }
    const [value, setValue] = useState()
    return (
        <div>
            {LoginModal && (
                <div className='flex inset-0 fixed justify-between items-center h-full bg-black bg-opacity-70'>
                    <div className='bg-white h-[50%] w-[50%] m-auto py-20 px-52 shadow-lg rounded-2xl flat'>
                        <div className=''>
                            <h1 className='absolute left-[76%] top-[25%] cursor-pointer text-red-600 Inter-Bold text-2xl' onClick={handleClose}>X</h1>
                        </div>
                        <div className='text-4xl text-center DM_Sans_Bold text-black Inter-SemiBold mb-20'>Successfully Create Account✅</div>
                        <div className='text-center flex justify-center'>
                            <button className='bg-red-600 py-3 px-16 flex justify-center rounded-lg cursor-pointer'><Link href="/" className='text-white'>👉 Click Me</Link></button>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}

export default Modal

