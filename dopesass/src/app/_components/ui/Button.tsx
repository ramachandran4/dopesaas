import Link from 'next/link'
import React, { useState } from 'react'
import Modal from '../modal/Modal';

const Button = ({ value, name }: any) => {
    const [isLoginModal, setIsLoginModal] = useState(false);

    // const handleClick = ({ value, name }: any) => {
    //     setIsLoginModal(!isLoginModal);
    // }

    // const handleClick = () => {
    //     console.log("Button clicked!");
    // };

    return (
        <div>
            <div className='py-3 px-28 bg-[#312ECB] w-full rounded-lg text-center cursor-pointer'>
                <Link href="" className='text-white Inter-SemiBold' onClick={(handleClick) => {
                    setIsLoginModal(!isLoginModal)
                }}>{value}{name}</Link>
            </div>
            {isLoginModal && (
                <div className="modal">
                    <div className="modal-content">
                        <Modal />
                    </div>
                </div>
            )}
        </div>
    )
}

export default Button
