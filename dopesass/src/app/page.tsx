import React from 'react'
import Login from './auth/login/page'
import SignUp from './auth/signup/page'
import Home from './_components/Home'

const page = ({children}: any) => {
	return (
		<>
			{children}
			<Home />
		</>
	)
}

export default page
